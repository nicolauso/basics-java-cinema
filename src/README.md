# CONSIGNES

Tu dois développer une partie du système de gestion du personnel pour un cinéma. Le programme doit te permettre de stocker des informations sur chaque employé et de calculer leur salaire en fonction des heures travaillées et de leur taux horaire.
Dans le main de ton programme Java, tu dois :
Créer quatre tableaux séparés pour stocker les informations relatives à cinq employés :


String[] employeeNames pour les noms, avec les valeurs "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"

int[] hoursWorked pour les heures travaillées pendant la semaine, avec les valeurs 35, 38, 35, 38, 40

double[] hourlyRates pour les taux horaires, avec les valeurs 12.5, 15.0, 13.5, 14.5, 13.0

String[] positions pour les postes, avec les valeurs "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"

À l'aide d'une boucle for-each, calcule et affiche le salaire hebdomadaire pour chaque employé. Le salaire est calculé comme le produit des heures travaillées et du taux horaire. Si un employé travaille plus de 35 heures, les heures supplémentaires sont comptées à 1.5 fois le taux horaire normal.
Ajoute une chaîne de caractères searchPosition, avec une valeur de ton choix parmi les suivantes : "Caissier", "Projectionniste" et "Manager"
Utilise une autre boucle for-each pour lister tous les noms de tous les employés dont le poste correspond à la valeur de searchPosition. Affiche un message "Aucun employé trouvé." dans le cas où il n'y a aucun résultat.
Compile et teste ton programme.