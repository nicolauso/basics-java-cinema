import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    static String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
    static int[] hoursWorked = {35, 38, 35, 38, 40};
    static double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
    static String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};
    static String searchPosition = "Perfectionniste";

    public static void main(String[] args) {

        ArrayList<String> arrayOfPositions = new ArrayList<>();

            for (int i = 0; i < employeeNames.length; i++) {
                double salary = hoursWorked[i] <= 35 ? hoursWorked[i] * hourlyRates[i] : (hoursWorked[i] * 35 + ((hourlyRates[i])-35) * 1.5) ;
                System.out.println(employeeNames[i] + ' ' + salary);
            }
            for(int i = 0; i < positions.length; i++) {
                if (searchPosition.equals(positions[i])) {
                    arrayOfPositions.add(employeeNames[i]);
                }
            }
            if (arrayOfPositions.isEmpty()) {
                System.out.println("Aucun résultat trouvé");
            } else {
                System.out.println(Arrays.toString(employeeNames));
            }
    }
}